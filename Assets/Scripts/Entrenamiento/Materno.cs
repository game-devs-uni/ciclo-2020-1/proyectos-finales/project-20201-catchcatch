﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Materno : MonoBehaviour
{
    [SerializeField] GameObject[] monsters = new GameObject[7];
    [SerializeField] GameObject[] Robots = new GameObject[7];
    [SerializeField] GameObject Canvas1, Canvas2, camaraCanvas, camara;
    [SerializeField] GameObject[] controls = new GameObject[3];
    Select select;
    public ControlEntreanmient control;
    public bool robot;
    [SerializeField] CMonster cMonster;
    private int cont;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (cMonster.Complete && cont == 0)
        {
            RobotEntr();
            cont++;
        }
        
        
    }

    public void iniciarMonster()
    {
        camara.gameObject.SetActive(false);
        select = new Select();
        Canvas1.gameObject.SetActive(false);
        Canvas2.gameObject.SetActive(true);
        camaraCanvas.gameObject.SetActive(false);   
        monsters[select.geteleccion()].gameObject.SetActive(true);

    }
    public void iniciarRobot()
    {
        camara.gameObject.SetActive(false);
        select = new Select();
        Canvas1.gameObject.SetActive(false);
        Canvas2.gameObject.SetActive(true);
        camaraCanvas.gameObject.SetActive(false);   
        Robots[select.geteleccion()].gameObject.SetActive(true);

    }


    void RobotEntr()
    {
        robot = true;
        camara.gameObject.SetActive(true);
        controls[0].gameObject.SetActive(false);
        Canvas1.gameObject.SetActive(true);
        Canvas2.gameObject.SetActive(false);
        camaraCanvas.gameObject.SetActive(true);
        monsters[select.geteleccion()].gameObject.SetActive(false);
        control.inciarSegunda();
    }
}
