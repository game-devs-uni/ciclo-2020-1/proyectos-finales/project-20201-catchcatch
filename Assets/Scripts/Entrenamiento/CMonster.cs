﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CMonster : MonoBehaviour
{
    [SerializeField] GameObject[] Afases = new GameObject[4];
    int fases = 0;
    public bool Complete;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Afases[fases].gameObject.SetActive(false);
            fases++;
        }

        if ( fases < 4)
        {
        
            Afases[fases].gameObject.SetActive(true);
        }
        else Complete = true;

    }
}
