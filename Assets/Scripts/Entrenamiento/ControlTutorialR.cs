﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlTutorialR : MonoBehaviour 
{
    [SerializeField] GameObject[] Robots = new GameObject[7];

    [SerializeField] GameObject PrimeraPantalla,Select;
    static int Sl;
    [SerializeField] Text nombre;
    [SerializeField] RectTransform rect;
    GameObject temporal;
   
    // Start is called before the first frame update
    void Start()
    {
        temporal = new GameObject();
    }
     void Update()
    {
      nombre.text = Robots[Sl].gameObject.name;
    }
    public void PrimerBoton()
    {
        PrimeraPantalla.gameObject.SetActive(false);
        
    }

    public void Continuar()
    {

    }
        
    public void eleccion(int selec)
    {
        Sl = selec;
        Select select = new Select();
        select.setEleccion(selec);
        PrimeraPantalla.gameObject.SetActive(false);
        Select.gameObject.SetActive(true);
        temporal = Instantiate(Robots[selec], rect.position,rect.rotation);
        temporal.transform.SetParent(rect);
        //temporal.transform.position = rect.transform.position;
        rect.localScale = new Vector3(0.05f, 0.05f,0.05f);
       // rect.position = new Vector3(0, -109f, 0f);
    }

    public void retornar()
    {
        Debug.Log("pin");
        Select.gameObject.SetActive(false);
        PrimeraPantalla.gameObject.SetActive(true);
        Destroy(temporal);
        rect.localScale = new Vector3(1, 1,1);
    }
    /*
    public void inciarSegunda()
    {
        Primera.gameObject.SetActive(false);
        Segunda.gameObject.SetActive(true);
    }
    */
}
