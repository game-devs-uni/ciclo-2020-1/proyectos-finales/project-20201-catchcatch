﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCamera : MonoBehaviour
{
    [SerializeField] GameObject Camera1, Camera2;
    private bool Camera;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.E))
        {
            if (Camera)
            {
                Camera1.gameObject.SetActive(false);
                Camera2.gameObject.SetActive(true);
                Camera = false;
            }
            else
            {
                Camera1.gameObject.SetActive(true);
                Camera2.gameObject.SetActive(false) ;
                Camera = true;
            }


        }
    }
}
