﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterMov : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]  float x, y, VelMov, VelRot,timeRealse;
    float VelMovOrig;
    Transform trans;
    Animator animator;
    Rigidbody rgb;


    void Start()
    {
        trans = this.gameObject.GetComponent<Transform>();
        animator = this.gameObject.GetComponent<Animator>();
        rgb = this.gameObject.GetComponent<Rigidbody>();
        VelMovOrig = VelMov;
    }

    // Update is called once per frame
    void Update()
    {

        this.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0, -9, 0));
        if (Input.GetKeyDown(KeyCode.Q))
        {
            timeRealse = 0;
            animator.SetBool("Attack", true);

        }
        if (timeRealse >= 1f)
        {
            animator.SetBool("Attack", false);
            x = Input.GetAxis("Horizontal");
            y = Input.GetAxis("Vertical");
            if (Input.GetKey(KeyCode.LeftShift))
            {
                VelMov = 0.7f* VelMovOrig;
                y = 0.5f * y;
                x = 0.5f * x;
            }

            if (y <= -0.8f) VelMov = 0.25f * VelMovOrig; 
            else VelMov = VelMovOrig; 

            trans.Translate(0, 0, y * Time.deltaTime * VelMov);
           // rgb.velocity = new Vector3(0,0, y * Time.deltaTime * VelMov);
            animator.SetFloat("VelY", y);
            trans.Rotate(0, x * Time.deltaTime * VelRot, 0);
            animator.SetFloat("VelX", x);
        }

        timeRealse += Time.deltaTime;

    }
}
