﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotBall
{

    Animator animator;
    bool Anim;
    Transform trans;
    float x, y, VelMov, VelRot;

    public void Constructor(Animator animator, Transform transform, float VelMov, float VelRot)
    {
        this.animator = animator;
        this.trans = transform;
        this.VelMov = VelMov;
        this.VelRot = VelRot;
    }

    public void setVelMov(float VelMov, float VelRot)
    {
        this.VelMov = VelMov;
        this.VelRot = VelRot;
    }

    public void funcion()
    {
        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");

        if (y >= 0.4f && x == 0)
        {
            trans.Translate(0, 0, y * Time.deltaTime * VelMov * 1.5f);
            animator.SetFloat("VelY", y);
            Anim = true;
        }
        else if (y <= -0.4f && x == -0)
        {
            trans.Translate(0, 0, y * Time.deltaTime * VelMov * 0.30f);
            animator.SetFloat("VelY", y);
            Anim = true;

        }
        else if ((x >= 0.4f || x <= -0.4f) && y == 0)
        {
            trans.Rotate(0, x * Time.deltaTime * VelRot, 0);
            animator.SetFloat("VelX", x);
            Anim = true;

        }
        if ((x >= 0.4f || x <= 0.4f) && (y >= 0.4f))
        {
            trans.Translate(0, 0, y * Time.deltaTime * VelMov *1.5f);
            trans.Rotate(0, x * Time.deltaTime * VelRot, 0);
            Anim = false;
        }

        if (!Anim)
        {
            animator.SetFloat("VelX", x);
            animator.SetFloat("VelY", y);
        }
        Anim = false;
    }

}
