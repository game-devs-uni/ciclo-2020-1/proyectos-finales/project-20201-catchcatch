﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovSphereRobot : MonoBehaviour
{
    [SerializeField] float x, y, VelRot, VelMov, timeRelase;
    [SerializeField] private Animator animator;
    Transform trans;
    RobotWalk robotWalk = new RobotWalk();
    RobotBall robotBall = new RobotBall();
   //[SerializeField] Collider colliderWalk, colliderBall;
    
    bool IsBall;
    // Start is called before the first frame update
    void Start()
    {

        animator = this.gameObject.GetComponent<Animator>();
        animator.SetBool("MovWalk", true);
        trans = this.gameObject.GetComponent<Transform>();
        robotWalk.Constructor(animator, trans, VelMov, VelRot);
        robotBall.Constructor(animator, trans, VelMov, VelRot);
        Physics.gravity = new Vector3(0, -9, 0);

    }
        
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            IsBall = !IsBall;
            robotBall.setVelMov(0, 0);
            robotWalk.setVelMov(0, 0);
            timeRelase = 0f;
        }

        if (timeRelase >= 2.5f)
        {
            robotWalk.setVelMov(VelMov,VelRot);
            robotBall.setVelMov(VelMov,VelRot);
        }


            if (!IsBall)
            {
           // colliderWalk.gameObject.SetActive(true);
           //  colliderBall.gameObject.SetActive(false);
                animator.SetBool("MovWalk", true);
                robotWalk.funcion();
            }
            else
            {
          //  colliderWalk.gameObject.SetActive(false);
         //   colliderBall.gameObject.SetActive(true);
            animator.SetBool("MovWalk", false);
                robotBall.funcion();

            }

        timeRelase += Time.deltaTime;


    }






}
