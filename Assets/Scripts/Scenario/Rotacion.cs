﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotacion : MonoBehaviour
{
    Transform trans;
    [SerializeField] float VelRot;
    // Start is called before the first frame update
    void Start()
    {
        trans = this.gameObject.GetComponent<Transform>();
        
    }

    // Update is called once per frame
    void Update()
    {

        trans.Rotate(0,1* Time.deltaTime * VelRot, 0);

    }
}
