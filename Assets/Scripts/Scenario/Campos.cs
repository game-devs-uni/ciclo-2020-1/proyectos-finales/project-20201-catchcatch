﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Campos : MonoBehaviour
{
    [SerializeField] GameObject CampoSuperior, CampoInferior;
    [SerializeField] Elevador elevador;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (elevador.getUP()) CampoSuperior.gameObject.SetActive(false);
        else CampoSuperior.gameObject.SetActive(true);

        if (elevador.getDown()) CampoInferior.gameObject.SetActive(false);
        else CampoInferior.gameObject.SetActive(true);
    
    }
}
